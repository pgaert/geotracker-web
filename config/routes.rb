Rails.application.routes.draw do
  scope '/log', controller: :points do
    get '' => :log, as: :point_log
    get '/:subject' => :create, as: :point_create
  end

  resources :subjects

  get '/map/:subject' => 'points#map', as: :point_map

  get '/points/:subject' => 'points#index', as: :points
end
