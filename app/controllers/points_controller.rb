class PointsController < ApplicationController
  def create
    params.require([:subject, :lat, :lon])

    extra_params = params.permit(:subject, :lat, :lon, :desc, :sat, :alt, :spd, :acc, :dir, :prov, :time,
                                 :start_time, :batt, :device_id, :serial, :act)
    extra_params[:time] ||= Time.now
    if extra_params[:start_time]
      extra_params[:start_time] = Time.at(extra_params[:start_time].to_i).utc.to_time
    end
    Point.create! extra_params
    head :no_content
  end

  def log
    render plain: point_create_url(subject: '') + '%AID?' +
        'lat=%LAT&lon=%LON&desc=%DESC&sat=%SAT&alt=%ALT&spd=%SPD&acc=%ACC&dir=%DIR&prov=%PROV&time=%TIME&start_time=%STARTTIMESTAMP&batt=%BATT&device_id=%AID&serial=%SERIAL&act=%ACT'
  end

  def map
    respond_to do |format|
      format.html {
        @subject = params.require(:subject)
        @points = Point.where(subject: @subject).where('time > ?', Time.now.beginning_of_day).order(:time).select(:lat, :lon).collect { |p| [p.lat, p.lon]}
        @lastPoint = Point.where(subject: @subject).order(id: :desc).first
      }
      format.json {
      }
    end

    render layout: false
  end

  def index
    @subject = params.require(:subject)
    @points = Point.where(subject: @subject).where('id > ?', params[:id]).order(:id)
    render json: @points
  end
end
