#!/bin/sh
cd /home/app
/usr/bin/bundle exec rake db:migrate
echo "$RAILS_MASTER_KEY" > config/master.key
chown -R app:app .