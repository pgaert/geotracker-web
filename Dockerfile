FROM phusion/passenger-ruby25:0.9.32
ENV RAILS_ENV=production

ADD Gemfile Gemfile.lock /home/app/
WORKDIR /home/app

RUN apt-get -qq update && apt-get -qqy install tzdata && gem install bundler && gem update --system && /usr/bin/bundle install --jobs 16 --retry 5

ADD . /home/app
RUN mv webapp.conf /etc/nginx/sites-enabled/webapp.conf && \
    mv rails-logrotate.conf /etc/logrotate.d/ && \
    rm /etc/nginx/sites-enabled/default && \
    rm -f /etc/service/nginx/down && \
    mv app_init.sh /etc/my_init.d/

CMD ["/sbin/my_init"]