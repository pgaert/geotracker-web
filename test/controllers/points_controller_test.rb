require 'test_helper'

class PointsControllerTest < ActionDispatch::IntegrationTest
  test "should get create" do
    assert_difference 'Point.count' do
      get point_create_url(subject: :foobar, params: {lat: 39.997298, lon: -105.292674, desc: 'GRG-CA'})
    end
    assert_response :success
  end

end
