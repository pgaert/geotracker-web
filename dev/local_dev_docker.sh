#!/bin/bash -ex
SCRIPTDIR=$(dirname "$(readlink -f "$0")")
APPDIR="$SCRIPTDIR/.."
time docker build -t gt-app $APPDIR
docker run -it --rm --net host -p 3000:3000 \
    -v $APPDIR/bin/docker_run.sh:/home/app/bin/docker_run.sh \
    -v $APPDIR/log:/home/app/log \
    -v $APPDIR/tmp:/home/app/tmp \
    -v $APPDIR/db/sqlite3:/home/app/db/sqlite3 \
    -v /tmp:/tmp \
    -e RAILS_MASTER_KEY=$(cat $APPDIR/config/master.key) \
     gt-app