class CreatePoints < ActiveRecord::Migration[5.2]
  def change
    create_table :points do |t|
      t.string :subject
      t.float :lat
      t.float :lon
      t.text :desc
      t.integer :sat
      t.integer :alt
      t.float :spd
      t.float :acc
      t.float :dir
      t.string :prov
      t.timestamp :time
      t.timestamp :start_time
      t.integer :batt
      t.string :device_id
      t.string :act
      t.string :serial

      t.timestamps
    end
  end
end
